import 'package:geolocator/geolocator.dart' as geo;
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class MyLocation {
  double? latitude2 = 37.5666103;
  double? longitude2 = 126.9783882;
  Future<void> getMyCurrentLocation() async {
    try {
      print('getMyCurrentLocation 함수 안, try 성공시');
      // 위치 권한 요청하는 코드
      geo.LocationPermission permission =
          await geo.Geolocator.requestPermission();
      print('위치권한 요청 완료,');
      geo.Position position = await geo.Geolocator.getCurrentPosition(
          desiredAccuracy: geo.LocationAccuracy.high);
      print('현재 위치 확인 완료,');
      latitude2 = position.latitude;
      longitude2 = position.longitude;
      print(latitude2);
      print(longitude2);
    } catch (e) {
      print('인터넷 연결에 문제가 발생했습니다.');
    }
  }
}
