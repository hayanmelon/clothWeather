import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Pallete{
  static const Color iconColor = Color(0xFFECF3F9); // Color(0xFF81CAFE);
  static const Color activeColor = Color(0xFF09126C);
  static const Color textColor1 = Color(0XFFA7BCC7);
  static const Color textColor2 = Color(0XFF9BB3C0);
  static const Color facebookColor = Color(0xFF3B5999);
  static const Color googleColor = Color(0xFFDE4B39);
  static const Color backgroundColor = Color(0xFF3B5999);//Color(0xFFCFD8DC);
  static const Color backgroundColorContainer = Color(0xFFECF3F9);
}

class IconPallete{
  Widget circleIconWeather(var content) {
    return Container(
      height: 60.0,
      width: 60.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Pallete.iconColor
      ),
      alignment: Alignment.center,
      child: content,
    );
  }

  Widget circleIconCloth(var content) {
    return Container(
      height: 60.0,
      width: 60.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Pallete.iconColor
      ),
      alignment: Alignment.center,
      child: content,
    );
  }
}

