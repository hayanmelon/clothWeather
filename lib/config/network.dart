import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class Network {
  // final String url;
  // Network(this.url);
  //
  Future<dynamic> getJsonData(var url) async {
    print('-- getJsonData 시작 -- ');
    //print(Uri.parse(url));
    //    Header set Access-Control-Allow-Origin "*"
    // Header set Access-Control-Allow-Headers "Content-Type"
    // Header set Referrer-Policy "no-referrer-when-downgrade"

    //
    try {
      final response = await http.get(url);
      print('response 요청 완료');
      print('response 요청 결과 : ${response.statusCode}');
      if (response.statusCode == 200) {
        String jsonData = response.body;
        var parsingData = jsonDecode(jsonData);
        return parsingData;
      } else {
        print('response 요청 결과 : ${response.statusCode}');
        // return response.statusCode;
        return null;
      }
    } catch (e) {
      print('getJsonData : 에러발생.');
      return null;
    }
  }

  Future<dynamic> getJsonData2(var url) async {
    print('-- getJsonData2 시작 -- ');
    //print(Uri.parse(url));
    http.Response response = await http.get(url, headers: {
      // "User-Agent":
      //     "PostmanRuntime/7.29.2", // Required for CORS support to work
      // "Accept": "*/*", // Required for cookies, authorization headers with HTTPS
      // "Accept-Encoding": "gzip, deflate, br",
    });
    print(response.statusCode);
    if (response.statusCode == 200) {
      String jsonData = response.body;
      var parsingData = jsonDecode(jsonData);
      return parsingData;
    } else {
      print(response.statusCode);
    }

    // try {
    //   var response = await http.get(url);
    //   print(response.statusCode);
    //   if (response.statusCode == 200) {
    //     String jsonData = response.body;
    //     var parsingData = jsonDecode(jsonData);
    //     return parsingData;
    //   } else {
    //     print(response.statusCode);
    //   }
    // } catch (e) {
    //   print('getJsonData : 에러발생.');
    //   return null;
    // }
  }
  // Future<dynamic> getJsonData(final String url, Map<String, String> body) async {
  //   print('-- getJsonData 시작 -- ');
  //   print(Uri.parse(url));

  //   try {
  //     http.Response response = await http.get(Uri.parse(url), headers: {
  //       "Access-Control-Allow-Origin": "*", // Required for CORS support to work
  //       "Access-Control-Allow-Credentials":
  //           "true", // Required for cookies, authorization headers with HTTPS
  //       "Access-Control-Allow-Headers":
  //           "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
  //       "Access-Control-Allow-Methods": "POST, OPTIONS"
  //     });
  //     if (response.statusCode == 200) {
  //       String jsonData = response.body;
  //       var parsingData = jsonDecode(jsonData);
  //       return parsingData;
  //     } else {
  //       print(response.statusCode);
  //     }
  //   } catch (e) {
  //     print('getJsonData : 에러발생으로 연습용 JSON 파일을 사용합니다.');
  //     var jsonText = await rootBundle.loadString('json/practice.json');
  //     var parsingData = jsonDecode(jsonText);
  //     return parsingData;
  //   }
  // }
}

// import 'package:http/http.dart' as http;
//
// class Network{
//   var url = Uri.https('apis.data.go.kr/1360000/VilageFcstInfoService_2.0/getUltraSrtNcst?', 'whatsit/create');
//   Future<dynamic> getJsonData() async {
//     http.Response response = await http.post(url, body : {
//       'apiKey' : 'sW7jJT03r%2FtDJKJ7gncwxq5awMJLIrjof0bjmI%2F%2Byba16fqtFwD83udZRNm1fi%2FXcfBc%2BcdIziWW2708n8Jbgw%3D%3D',
//       'pageNo' : '1',
//       'numOfRows' : '1000',
//       'dataType' : 'JSON',
//       'base_date' : '20220825',
//       'base_time' : '0600',
//       'nx' : '55',
//       'ny' : '127',
//     }
//     );
//     print('Response status : ${response.statusCode}');
//     print('Response body : ${response.body}');
//   }
//
// }
