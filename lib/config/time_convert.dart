import 'package:flutter/material.dart';


String convertDate(DateTime dateTime){
  '${dateTime.year}${dateTime.month}';
  String? month1;
  String? day1;
  (dateTime.month < 10) ? month1 = '0'+ ((dateTime.month).toString()) : month1 = (dateTime.month).toString();
  (dateTime.day < 10) ? day1 =  '0'+ ((dateTime.day).toString()) : day1 = (dateTime.day).toString();
  String dateString = '${dateTime.year}' + month1 + day1;
  print(dateString);
  return dateString;
}

String convertTime(DateTime datetime){
  String hour1 = (datetime.hour.toString());
  String minute1 = (datetime.minute.toString());
  if (int.parse(minute1) < 40){
    hour1 = (int.parse(hour1) -1).toString();
    minute1 = '00';
  }
  else minute1 = '00';

  if (hour1.length == 1) hour1 = '0' + hour1;
  String timeString = '${hour1}${minute1}';
  print(timeString);
  return timeString;
}