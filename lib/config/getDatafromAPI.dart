import 'dart:io';

import 'package:cloth_weather/config/mylocation.dart';
import 'package:cloth_weather/config/pallete.dart';
import 'package:cloth_weather/config/time_convert.dart';
import 'package:cloth_weather/packages/convert_grid.dart';
import 'package:cloth_weather/widgets/iconwidgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloth_weather/config/decode_codes.dart';
import 'package:cloth_weather/config/network.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:cloth_weather/config/time_convert.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloth_weather/widgets/temp_cloth_corner.dart';
import 'package:flutter/src/services/asset_bundle.dart';

class GetDataFromApi with ChangeNotifier {
  // 공공데이터 포털 키
  Map<String, String> apiKey_data_go_kr = {
    'serviceKey':
    'sW7jJT03r/tDJKJ7gncwxq5awMJLIrjof0bjmI/+yba16fqtFwD83udZRNm1fi/XcfBc+cdIziWW2708n8Jbgw==',
  };
  // 네이버 API 키
  Map<String, String> apiKey_naver = {
    'X-NCP-APIGW-API-KEY-ID': '0bk0jig2eo',
    'X-NCP-APIGW-API-KEY': 'PjE0PjQMZ84IqaItRreELZLsIhh47yy98fCB7f5O',
  };

  // 필요한 변수 정의
  //getDataFromApi(this.weatherTime, this.weatherType, this.weatherTemp);
  double? latitude = 37.5666103;
  double? longitude = 126.9783882;
  List<String> weatherTime = ['에러1', '에러2', '에러3', '에러4', '에러5', '에러6', '에러7'];
  List<String> weatherType = ['10', '10', '10', '10', '10', '10', '10'];
  List<String> weatherTemp = ['99', '99', '99', '99', '99', '99', '99'];
  List<String> nowAddress = [' '];

  bool isDataLoad1 = false;
  bool isDataLoad2 = false;
  bool isDataLoad3 = false;

  // getDatas : 네이버API 주소 요청, 날씨 초단기실황 요청, 초단기예보 요청,
  // 10분마다 업데이트 되므로 10분 기준 업데이트 해준다!
  Future getDatas() async {
    getCoord(); // 위경도계 좌표를 받아온다.

    var parsingResult;
    // 현재 위치의 주소정보를 get 하는 부분
    // 네이버 지도 API를 사용하여 주소 정보를 get 함.
    // getAddrData 함수 사용.
    print('네이버 지도 정보 get 시작');
    parsingResult = await getAddrData();
    isDataLoad1 = false;
    if (parsingResult != null) isDataLoad1 = true;
    print(DateTime.now());

    String codeSKY = '';
    String codePTY = '';

    // 초단기 실황(현재날씨) 데이터를 요청한다
    parsingResult = await getWeatherData('getUltraSrtNcst');
    print('초단기 실황 요청 완료');
    print(parsingResult);
    // 초단기 실황(현재 날씨) 요청 후 값이 제대로 들어오면,
    // 변수들을 초기화 하고 0번째 값에 날짜 데이터들을 입력한다
    // 날짜정보 리스트의 0번째 값은 모두 현재 날씨 데이터임.
    isDataLoad2 = false;
    if (parsingResult != null) {
      isDataLoad2 = true; // 데이터 로딩이 정상적으로 되었는지 판단하는 불린변수임.
      weatherType.clear();
      weatherTime.clear();
      weatherTemp.clear();
      weatherTemp.add((double.parse(parsingResult["response"]["body"]["items"]
      ['item'][3]['obsrValue'])
          .round())
          .toString());
      weatherType.add("");
      weatherTime.add("");
    }

    // 초단기예보(앞으로 6시간 예보) 데이터를 요청한다.
    parsingResult = await getWeatherData('getUltraSrtFcst');
    print('초단기 에보 요청 완료');
    // 초단기 예보(앞으로 6시간 날씨) 요청 후 값이 제대로 들어오면,
    // 날씨정보 리스트에 앞으로 6시간 날씨값들을 순서대로 추가한다.
    isDataLoad3 = false;
    if (parsingResult != null) {
      isDataLoad3 = true;
      print('--- next weather list ----');
      for (int i = 24; i < 30; i++) {
        weatherTemp.add(
            parsingResult["response"]["body"]["items"]['item'][i]['fcstValue']);
        codeSKY = parsingResult["response"]["body"]["items"]['item'][i - 6]
        ['fcstValue'];
        codePTY = parsingResult["response"]["body"]["items"]['item'][i - 18]
        ['fcstValue'];
        weatherTime.add(
            parsingResult["response"]["body"]["items"]['item'][i]['fcstTime']);
        print(codeSKY);
        weatherType.add(decodeSKY(SKY: codeSKY, PTY: codePTY));
      }
      weatherType[0] = weatherType[1];
    }
    print(weatherTemp);
    print(weatherTime);
    print(weatherType);
    print(isDataLoad1 & isDataLoad2 & isDataLoad3);
  }

  // getCoord : 위경도계 좌표를 받아온다
  Future getCoord() async{
    //MyLocation 함수를 이용해 현재 위치를 위도경도 좌표계로 받아온다.
    MyLocation myLocation = MyLocation();
    await myLocation.getMyCurrentLocation();
    longitude = myLocation.longitude2;
    latitude = myLocation.latitude2;
  }

  // 공공데이터포털에서 xy좌표계를 사용하여 날씨정보를 가져온다.
  // type : 공공데이터포털에서 요청할 타입
  // type 종류 : getUltraSrtFcst(초단기예측), getUltraSrtNcst(초단기실황)
  Future<dynamic> getWeatherData(String type) async {
    String url1 = 'apis.data.go.kr';
    String url2 = '1360000/VilageFcstInfoService_2.0/$type';
    var gpsToGridData = ConvGridGps.gpsToGRID(latitude!, longitude!);

    print(gpsToGridData);

    // 공공데이터 포털 요청 url 파라미터
    Map<String, String> urlParams = {
      'pageNo': '1',
      'numOfRows': '1000',
      'dataType': 'JSON',
      'base_date': convertDate(DateTime.now()),
      'base_time': convertTime(DateTime.now()),
      'nx': gpsToGridData['x'].toString(),
      'ny': gpsToGridData['y'].toString(),
    };
    urlParams.addAll(apiKey_data_go_kr);

    print('urlParams : ${urlParams}');

    var url_result = Uri.https(url1, url2, urlParams);
    print(url_result);
    Network network = Network();
    var parsingResult = await network.getJsonData(url_result);
    return parsingResult;
  }

  // getAddrData = 위도경도 좌표계를 통해 그 위치의 주소를 얻어오는 함수. 네이버 API 사용.
  Future<dynamic> getAddrData() async {
    // 사용함수 : newtwork 파일 클래스의 getJsonData 함수 사용
    String url1 = 'naveropenapi.apigw.ntruss.com';
    String url2 = 'map-reversegeocode/v2/gc';
    var gpsToGridData = ConvGridGps.gpsToGRID(latitude!, longitude!);
    // 네이버 엔클라우드  url 파라미터
    Map<String, String> urlParams = {
      'request': 'coordsToaddr',
      'coords': '$longitude,$latitude',
      'output': 'json',
      'orders': 'addr,admcode',
    };
    urlParams.addAll(apiKey_naver);

    var url_result = Uri.https(url1, url2, urlParams);
    print(url_result);
    Network network = Network();
    var parsingData = await network.getJsonData(url_result);

    //
    if (parsingData != null) {
      nowAddress = [];
      var temp = '';
      for (int i = 1; i < 5; i++) {
        temp = parsingData['results'][0]['region']['area' + (i.toString())]
        ['name'];
        temp == "" ? null : nowAddress.add(temp);
      }
      print(nowAddress.reversed.first);
      //print(parsingData);
      return parsingData;
    } else {
      return null;
    }
  }
}
