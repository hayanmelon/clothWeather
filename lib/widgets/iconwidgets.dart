import 'package:flutter/material.dart';

import '../config/pallete.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget weatherIconsSet(
    {List<String>? weatherTime,
    List<String>? weatherTemp,
    List<String>? weatherType}) {
  List<Widget> arrays = [];

  for (int i = 1; i < weatherTime!.length; i++) {
    arrays.add(weatherIconsWidget(
      weatherTime: weatherTime[i],
      weatherTemp: weatherTemp![i],
      weatherType: weatherType![i],
    ));
    arrays.add(SizedBox(
      width: 10,
    ));
  }

  print(weatherTemp!.length);
  return Row(
    children: arrays,
  );
}

Widget weatherIconsWidget(
    {String weatherTemp = '',
    String weatherType = '',
    String weatherTime = ''}) {
  return Column(
    children: [
      IconPallete().circleIconWeather(
        SvgPicture.asset(
          'assets/icons/${weatherType}.svg',
          color: Colors.indigo,
        ),
      ),
      SizedBox(
        height: 10,
      ),
      Text(weatherTemp + 'º'),
      SizedBox(
        height: 5,
      ),
      Text(weatherTime.substring(0, 2) + "시"),
    ],
  );
}
