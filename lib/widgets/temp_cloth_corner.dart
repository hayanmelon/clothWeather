import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../config/pallete.dart';
import 'package:flutter_svg/svg.dart';

class tempClothCorner {
  // static String jsonString = '/assets/cloth_name.json';
  // static Map<String,dynamic> jsonData = jsonDecode(jsonString);
  static Map<String, dynamic> jsonData = {'나시':'07'};

  static void jsonInitialize() async{
    String jsonString = await rootBundle.loadString('assets/json/cloth_name.json');
    jsonData = json.decode(jsonString);
  }

  //Map<String, String>
  static Widget itemColumns(int targetTemp){
    jsonInitialize();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        itemRows(27,99, ['나시','민소매','반바지'], targetTemp),
        itemRows(23,26, ['반팔티','얇은셔츠','얇은긴팔','반바지','면바지'], targetTemp),
        itemRows(20,22, ['긴팔티','가디건','후드티','면바지','슬랙스'], targetTemp),
        itemRows(17,19, ['니트','가디건','후드티','맨투','청바지','슬랙스','면바지','원피스'], targetTemp),
        itemRows(12,16, ['자켓','셔츠','간절기야상','스타킹','레깅스'], targetTemp),
        itemRows(10,11, ['트렌치코트','간절기야상','패딩조끼'], targetTemp),
        itemRows(6,9, ['코트','가죽자켓','야상','겨울바지','두꺼운면바지'], targetTemp),
        itemRows(0,5, ['패딩','목도리','두꺼운 야상','귀도리','내복'], targetTemp),

      ],
    );
  }
  static Widget itemRows(int start, int end, List<String> items, int targetTemp) {
    List<Widget> colItems = [];
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children : [
        Column(
          children: [
            start <= 0 ? Text('~ $end') :Text('$start ~'),
            SizedBox(
              height: 10,
            ),
            end >= 99 || start <=0 ? Text('') : Text('$end'),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        for(int i=0 ;i< items.length; i++)...[
          SizedBox(width: 10,),
          Column(
            children: [
              IconPallete().circleIconCloth(
                Image(
                  image: (jsonData[items[i]]==null)? AssetImage('assets/peeps/${jsonData['나시']}.png') : AssetImage('assets/peeps/${jsonData['나시']}.png'),
                  width: 50,
                  height: 50,
                )
              ),
              SizedBox(
                height: 10,
              ),
              Text('${items[i]}',
                style: (targetTemp >= start && targetTemp <=end) ? TextStyle(
                    fontWeight: FontWeight.bold,
                ) : null
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ],
      ],
    );
  }
}