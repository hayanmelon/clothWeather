import 'dart:async';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:cloth_weather/config/mylocation.dart';
import 'package:cloth_weather/config/pallete.dart';
import 'package:cloth_weather/config/time_convert.dart';
import 'package:cloth_weather/packages/convert_grid.dart';
import 'package:cloth_weather/widgets/iconwidgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'config/decode_codes.dart';
import 'config/network.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'config/time_convert.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'widgets/temp_cloth_corner.dart';
import 'package:flutter/src/services/asset_bundle.dart';
import 'package:cloth_weather/config/getDatafromAPI.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // static GetDataFromApi? get_data = GetDataFromApi();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<GetDataFromApi>(
          create: (context) => GetDataFromApi(),
        ),
        FutureProvider<Map<String, List<String>>>(
          initialData: {
            'weatherType' : [' '],
            'weatherTime' : [' '],
            'weatherTemp' : [' '],
          },
          create: (context) async{
            await context.watch<GetDataFromApi>().getDatas();
            print('퓨처프로바이더 실행');
            return {
              'weatherTime' : context.watch<GetDataFromApi>().weatherTime,
              'weatherType' : context.watch<GetDataFromApi>().weatherType,
              'weatherTemp' : context.watch<GetDataFromApi>().weatherTemp,
            };
          },
        ),
      ],
      builder: (context, child) {
        return Scaffold(
          // appBar: AppBar(
          //   title: Text('cloth weather'),
          //   backgroundColor: Pallete.backgroundColorContainer,
          // ),
          backgroundColor: Pallete.backgroundColor,
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  // 날씨구역
                  Container(
                    padding: EdgeInsets.all(20.0),
                    margin: EdgeInsets.all(10.0),
                    color: Pallete.backgroundColorContainer,
                    child: Column(
                      children: [
                        // 동네버튼
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ElevatedButton(
                                onPressed: () async {
                                  //
                                  // final timer =
                                  // Timer(const Duration(seconds: 5), () => get_data!.getDatas());

                                  await Provider.of<GetDataFromApi>(context,
                                          listen: false)
                                      .getDatas();
                                  //await get_data!.getDatas();
                                  //setState(() {});
                                },
                                child: Text('남산동')),
                            ElevatedButton(
                                onPressed: () {
                                  setState(() {

                                  });
                                }, child: Text('대명동')),
                            ElevatedButton(
                                onPressed: () {}, child: Text('재마루길')),
                            ElevatedButton(
                                onPressed: () {}, child: Text('ㅁ??')),
                          ],
                        ),
                        // 현재 위치 및 날씨 큰 아이콘
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    context
                                                .watch<GetDataFromApi>()
                                                .nowAddress
                                                .length ==
                                            0
                                        ? 'Loading'
                                        : (context
                                                .watch<GetDataFromApi>()
                                                .nowAddress)
                                            .reversed
                                            .first,
                                    style: TextStyle(fontSize: 40),
                                  ),
                                  Text(context.read<Map<String, List<String>>>()['weatherTemp']![0],
                                    style: TextStyle(fontSize: 60),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: 150,
                                    width: 150,
                                    alignment: Alignment.center,
                                    child: SvgPicture.asset(
                                      'assets/icons/${context.watch<GetDataFromApi>().weatherType[0]}.svg',
                                      color: Colors.indigo,
                                      height: 120,
                                      width: 120,
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                          '${(DateTime.now()).year}-${(DateTime.now()).month}-${(DateTime.now()).day} \n ${(DateTime.now()).hour}:${(DateTime.now()).minute} 기준 '),
                                      IconButton(
                                          onPressed: () {
                                            setState(() {});
                                          },
                                          icon: Icon(
                                            Icons.refresh,
                                          )),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Row()
                      ],
                    ),
                  ),
                  Text(
                    ' ---- 광고 자리 ----',
                  ),
                  // 예보구역
                  Container(
                    margin: EdgeInsets.all(10.0),
                    //color: Pallete.backgroundColorContainer,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                              Colors.yellow,
                              Colors.deepPurple,
                            ])),
                        child: Row(
                          children: <Widget>[
                            // 날씨 아이콘 세트
                            weatherIconsSet(
                                weatherTemp:
                                    context.watch<GetDataFromApi>().weatherTemp,
                                weatherTime:
                                    context.watch<GetDataFromApi>().weatherTime,
                                weatherType: context
                                    .watch<GetDataFromApi>()
                                    .weatherType),
                          ],
                        ),
                      ),
                    ),
                  ),

                  // 옷차림구역
                  Container(
                    height: 300,
                    margin: EdgeInsets.all(10),
                    //color: Pallete.backgroundColorContainer,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.red,
                                Colors.yellow,
                                Colors.blue,
                              ],
                            )),
                            child: tempClothCorner.itemColumns(int.parse(context
                                .watch<GetDataFromApi>()
                                .weatherTemp[0]))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
